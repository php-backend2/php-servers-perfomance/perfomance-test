# Perfomance-test

## Configuring grafana datasources

When performing load testing projects on remote server make sure to set proper 
`DATASOURCE_INFLUXDB_URL` and `DATASOURCE_PROMETHEUS_URL` variable values at `docker-compose.yml` file

>Usually, when load testing is being performed on local computer, there no need to change configuration