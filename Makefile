BASE_COMMAND:=DOCKER_BUILDKIT=1 docker compose

TEST_ID:= ramping-$(shell date +"%Y-%m-%d-%T")

build:
	$(BASE_COMMAND) build

up:
	$(BASE_COMMAND) up -d grafana influxdb

ps:
	$(BASE_COMMAND) ps -a

down:
	$(BASE_COMMAND) down -v --remove-orphans --timeout=0

k6-run:
	$(BASE_COMMAND) run --rm k6 run /scripts/ramping.js --tag testid=$(TEST_ID)