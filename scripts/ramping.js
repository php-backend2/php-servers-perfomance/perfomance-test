import http from "k6/http";
import { check, sleep } from "k6";
import { Rate } from 'k6/metrics';
import { randomIntBetween, randomItem } from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';

const BASE_URL = 'http://85.209.9.179:8000/api';
// const BASE_URL = 'http://host.docker.internal:8000/api';
export const errorRate = new Rate('non_200_requests');

export const options = {
    discardResponseBodies: true,
    scenarios: {
        contacts: {
            executor: 'ramping-vus',
            startVUs: 5,
            stages: [
                { duration: '5s', target: 5 },
                { duration: '1m', target: 50 },
                { duration: '3m', target: 100 },
                { duration: '1m', target: 50 },
                { duration: '15s', target: 15 },
            ],
            gracefulRampDown: '0s',
        },
    },
};

// Simulated user behavior
export default function () {
    let limit = randomIntBetween(10,20);

    let usersUrl = BASE_URL + '/users/' + limit;
    let nearestPostOffice = BASE_URL + '/nearest-post-office';
    let sockColorsUrl = BASE_URL + '/sock-colors/' + limit;
    let userSocksUrl = BASE_URL + '/user-socks/' + limit;

    let req = randomItem([usersUrl, sockColorsUrl, userSocksUrl, nearestPostOffice])
    let res = http.get(req);

    check(res, { "status was 200": (r) => r.status === 200 }) || errorRate.add(1);
    sleep(Math.random() + 1);
}